OIDC Debugging Tool for Go
==========================

This Go (Golang) application serves as a debugging tool for OpenID Connect (OIDC) authentication. It allows you to quickly test OIDC authentication and inspect the authentication flow, token claims, and user information returned by the OIDC provider.

Purpose
-------

The OIDC Debugging Tool is designed to help developers:

- Test OIDC authentication configurations.
- Examine the authentication flow with ease.
- Verify the content of access tokens and token claims.
- View user information retrieved from the OIDC provider.

Prerequisites
-------------

Before using the tool, ensure you have the following prerequisites:

- Go (Golang) installed on your system.
- A Keycloak client with the "Direct access grants" enabled
- A valid OIDC configuration file (config.json) with the issuer URL, client ID, client secret, and scopes.

Usage
-----

1. Clone this repository.

2. Navigate to the project directory.

3. Create a configuration file named `config.json` with the following structure and populate it with your OIDC configuration:

    ```json
    {
        "issuerURL": "YOUR_OIDC_ISSUER_URL",
        "clientID": "YOUR_CLIENT_ID",
        "clientSecret": "YOUR_CLIENT_SECRET",
        "scopes": ["openid", "profile", "email"]
    }
    ```

4. Run the tool:

    ```
    go run main.go
    ```

5. Follow the on-screen prompts to enter your email and password for authentication.

6. The tool will authenticate, retrieve an access token, and display the access token, token claims, and user information for debugging purposes.

Features
--------

- Quick and easy OIDC authentication testing.
- Visual representation of the authentication flow.
- Detailed display of access token claims and user information.
