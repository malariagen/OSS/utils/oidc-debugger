module gkda

go 1.19

require (
	github.com/coreos/go-oidc/v3 v3.6.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.15.0
	golang.org/x/oauth2 v0.12.0
	golang.org/x/term v0.12.0
)

require (
	github.com/go-jose/go-jose/v3 v3.0.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
