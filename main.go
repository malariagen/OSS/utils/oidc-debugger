package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/dgrijalva/jwt-go"
	"github.com/fatih/color"
	"golang.org/x/oauth2"
	"golang.org/x/term"
)

type Config struct {
	IssuerURL    string   `json:"issuerURL"`
	ClientID     string   `json:"clientID"`
	ClientSecret string   `json:"clientSecret"`
	ClientScopes []string `json:"scopes"`
}

func readConfig(filename string) (Config, error) {
	var config Config
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return config, err
	}
	err = json.Unmarshal(data, &config)
	return config, err
}

func main() {
	config, err := readConfig("config.json")
	if err != nil {
		log.Fatal("Failed to read configuration:", err)
	}

	greenTitle := color.New(color.FgGreen)
	ctx := context.Background()

	username := os.Getenv("OIDC_USERNAME")
	if username == "" {
		fmt.Print("Enter email: ")
		fmt.Scan(&username)
	}

	var password string
	passwordEnv := os.Getenv("OIDC_PASSWORD")
	if passwordEnv == "" {
		fmt.Print("Enter password: ")
		passwordBytes, err := term.ReadPassword(int(syscall.Stdin))
		if err != nil {
			log.Fatal("Failed to read password:", err)
		}
		password = string(passwordBytes)
	} else {
		password = passwordEnv
	}

	provider, err := oidc.NewProvider(ctx, config.IssuerURL)
	if err != nil {
		log.Fatal(err)
	}

	configOAuth2 := oauth2.Config{
		ClientID:     config.ClientID,
		ClientSecret: config.ClientSecret,
		Endpoint:     provider.Endpoint(),
		Scopes:       config.ClientScopes,
	}

	token, err := configOAuth2.PasswordCredentialsToken(ctx, username, password)
	if err != nil {
		log.Fatal("Failed to authenticate:", err)
	}

	tokenJSON, err := json.MarshalIndent(token, "", "    ")
	if err != nil {
		log.Fatal("Failed to marshal token into JSON:", err)
	}

	fmt.Println(`======= TOKEN =======`)
	greenTitle.Println(string(tokenJSON))
	fmt.Println("")

	claims := jwt.MapClaims{}
	_, _, err = new(jwt.Parser).ParseUnverified(token.AccessToken, &claims)
	if err != nil {
		log.Fatal("Failed to decode token claims:", err)
	}

	fmt.Println("===== TOKEN CLAIMS =====")
	for key, value := range claims {
		greenTitle.Printf("%s: %v\n", key, value)
	}

	fmt.Println("")

	userInfo, err := provider.UserInfo(ctx, oauth2.StaticTokenSource(token))
	if err != nil {
		log.Fatal("Failed to get userinfo:", err)
	}

	var userInfoData map[string]interface{}
	if err := userInfo.Claims(&userInfoData); err != nil {
		log.Fatal("Failed to get userinfo:", err)
	}

	fmt.Println("==== USERINFO DATA ====")
	for key, value := range userInfoData {
		greenTitle.Printf("%s: %v\n", key, value)
	}

}
